# Análise do dataset GreenHub

Repositório contendo algumas análises referente ao dataset GreenHub.

## Como usar

### Carregando o dataset

É possível utilizar uma lista de dicionários para carregar os arquivos do
dataset, onde cada dicionário representa as informações de um arquivo e deverá
possuir a estrutura utilizada abaixo:

```python
import src.loader as loader

path = '.'
# Caminho para a pasta contendo os arquivos do dataset

df = loader.load_df(path, [
    {
        'name': ...,
        # Nome do arquivo a ser lido do dataset
        
        'cols': [
            'name': ...,
            # Nome da coluna a ser incluída

            'dtype': ...,
            # Tipo de valor da coluna (e.g. int, bool), opcional
            
            'converters': ..., 
            # Função para converter os valores da coluna antes de adicioná-los
            # ao DataFrame (e.g. str.lower), opcional
                    
            'preprocessor': ...,
            # Função para converter os valores da coluna após adicionálos ao
            # DataFrame (e.g. pd.to_datetime), opcional

            'rename': ...,
            # Novo nome da coluna, opcional. Colunas iguais de arquivos
            # diferentes serão usadas para mesclar os arquivos no DataFrame
        ]
    },
])
```

### Extraindo curvas do dataset

```python
df_curves = loader.extract_curves(df)
```

Após a extração de curvas, será gerado um novo dataset subtituíndo as colunas
**timestamp**, **battery_state** e **battery_level** por duas colunas **x** e 
**y**, mantendo o restante das colunas. Cada célula da coluna **x** contém uma
lista de listas, onde cada lista representa o domínio do tempo, em segundos, 
de uma curva de descarregamento do seu respectivo dispositivo. Da mesma forma,
cada célula da coluna **y** contém uma lista de listas, onde cada lista
representa o nível de bateria, em porcentagem, de uma curva de descarregamento
do seu respectivo dispositivo.

Baseado nessas curvas, é possível

#### selecionar as marcas, modelos e versões com maiores curvas

```python
groups = df_curves.groupby(['brand'])
df_brands = pd.DataFrame()
df_brands['num_curves'] = groups['x'].apply(len)
df_brands.sort_values('num_curves', ascending=False, inplace=True)
brands = df_brands.index[:4].values.tolist()

groups = df_curves.groupby(['brand', 'model'])
df_brands = pd.DataFrame()
df_brands['num_curves'] = groups['x'].apply(len)
df_brands.sort_values('num_curves', ascending=False, inplace=True)
models = [model for brand in brands for model in df_brands.loc[brand].index[:4]]

groups = df_curves.groupby(['brand', 'os_version'])
df_brands = pd.DataFrame()
df_brands['num_curves'] = groups['x'].apply(len)
df_brands.sort_values('num_curves', ascending=False, inplace=True)
versions = sorted({version for brand in brands \
            for version in df_brands.loc[brand].index[:4]})
```

#### selecionar as curvas das maiores marcas, modelos e versões

```python
df_brand_curves = df_curves[df_curves['brand'].isin(brands)]
df_model_curves = df_curves[df_curves['model'].isin(models)]
df_version_curves = df_curves[df_curves['os_version'].isin(versions)]
```

- Exemplo de `df_brand_curves`:

| device_id | x                                                 | y                                                 | model    | brand   | os_version |
|-----------|---------------------------------------------------|---------------------------------------------------|----------|---------|------------|
| 1         | [[0.0, 282.0, 533.0, 1279.0, 1523.0, 3725.0], ... | [[99, 98, 97, 96, 95, 93], [96, 95, 94, 93, 92... | vs500pp  | lge     | 6.0.1      |
| 5         | [[0.0, 21192.0, 25822.0, 57617.0, 59340.0, 610... | [[57, 53, 50, 38, 32, 25, 21], [67, 66, 65, 64... | lg-d331  | lge     | 4.4.2      |
| 9         | [[0.0, 3207.0, 3771.0, 7529.0, 8157.0, 8487.0,... | [[90, 89, 88, 87, 86, 85, 84, 83], [99, 98, 97... | sm-g903f | samsung | 6.0.1      |
| 16        | [[0.0, 524.0, 765.0, 61841.0, 62706.0, 65366.0... | [[99, 98, 97, 59, 57, 56], [56, 55, 54, 53, 52... | sm-g950f | samsung | 7.0        |
| ...       | ...                                               | ...                                               | ...      | ...     | ...        |

#### agrupar as curvas dos dispositivos das maiores marcas, modelos e versões

```python
def group_by_type(df: pd.DataFrame, *, by: str) -> pd.DataFrame:
    groups = df.groupby(by)
    df_grouped = pd.DataFrame()
    df_grouped['x'] = groups['x'].sum()
    df_grouped['y'] = groups['y'].sum()
    df_grouped['num_curves'] = groups['x'].count()
    return df_grouped

df_brand_grouped = group_by_type(df_brand_curves, by='brand')
df_model_grouped = group_by_type(df_model_curves, by='model')
df_version_grouped = group_by_type(df_version_curves, by='os_version')
```

- Exemplo de `df_brand_grouped`:

| brand    | x                                                 | y                                                 | num_curves |
|----------|---------------------------------------------------|---------------------------------------------------|------------|
| lenovo   | [[0.0, 111.0, 1239.0, 1594.0, 9635.0, 20536.0]... | [[46, 45, 44, 43, 42, 41], [87, 86, 85, 84, 83... | 766        |
| lge      | [[0.0, 282.0, 533.0, 1279.0, 1523.0, 3725.0], ... | [[99, 98, 97, 96, 95, 93], [96, 95, 94, 93, 92... | 1310       |
| motorola | [[0.0, 79.0, 186.0, 427.0, 567.0, 808.0, 977.0... | [[87, 86, 85, 84, 83, 82, 81, 42, 41, 40, 39, ... | 785        |
| samsung  | [[0.0, 3207.0, 3771.0, 7529.0, 8157.0, 8487.0,... | [[90, 89, 88, 87, 86, 85, 84, 83], [99, 98, 97... | 6570       |

#### agrupar as curvas dos dispositivos por uma coluna

```python
def group_by_column(df, *, column: str, by: str):
    groups = df.groupby([column, by])
    df_column_grouped = pd.DataFrame()
    df_column_grouped['x'] = groups['x'].sum()
    df_column_grouped['y'] = groups['y'].sum()
    df_column_grouped['num_curves'] = groups['x'].count()
    return df_column_grouped

df_bt_brand_grouped = group_by_column(df_brand_curves, column='bluetooth_enabled', by='brand')
df_bt_model_grouped = group_by_column(df_model_curves, column='bluetooth_enabled', by='model')
df_bt_version_grouped = group_by_column(df_version_curves, column='bluetooth_enabled', by='os_version')
```

- Exemplo de `df_bt_brand_grouped`:

| bluetooth_enabled | brand    | x                                                 | y                                                 | num_curves |
|-------------------|----------|---------------------------------------------------|---------------------------------------------------|------------|
| False             | lenovo   | [[0.0, 111.0, 1239.0, 1594.0, 9635.0, 20536.0]... | [[46, 45, 44, 43, 42, 41], [87, 86, 85, 84, 83... | 620        |
| False             | lge      | [[0.0, 282.0, 533.0, 1279.0, 1523.0, 3725.0], ... | [[99, 98, 97, 96, 95, 93], [96, 95, 94, 93, 92... | 1050       |
| False             | motorola | [[0.0, 79.0, 186.0, 427.0, 567.0, 808.0, 977.0... | [[87, 86, 85, 84, 83, 82, 81, 42, 41, 40, 39, ... | 624        |
| False             | samsung  | [[0.0, 3207.0, 3771.0, 7529.0, 8157.0, 8487.0,... | [[90, 89, 88, 87, 86, 85, 84, 83], [99, 98, 97... | 5220       |
| True              | lenovo   | [[0.0, 30.0, 60.0, 91.0, 121.0, 151.0, 181.0, ... | [[18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8], [... | 146        |
| True              | lge      | [[0.0, 35.0, 427.0, 1059.0, 1560.0, 1841.0, 23... | [[65, 64, 63, 62, 61, 60, 59, 57, 56, 55, 54, ... | 260        |
| True              | motorola | [[0.0, 1925.0, 2050.0, 2214.0, 2931.0, 3327.0,... | [[100, 99, 98, 97, 96, 95, 94, 93, 92, 91, 90,... | 161        |
| True              | samsung  | [[0.0, 1317.0, 4909.0, 12131.0, 12694.0, 13389... | [[68, 64, 53, 35, 34, 33, 21, 20, 19, 17, 16],... | 1350       |