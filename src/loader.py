import numpy as np
import pandas as pd

from .battery import split_curves
from .util.splitter import Splitter
from .util.array import Array

import functools
import datetime

def load_df(path: str, file_configs: list) -> pd.DataFrame:
    """
    Load a pandas.DataFrame based on dataset files.

    Parameters
    ----------
    path : str
        Path where the dataset files are located.

    file_configs : list
        Settings of which files to use to create the DataFrame. This parameter
        must be a list of dictionaries, where each dictionary must have at least
        two keys: 'name' and 'cols'. The 'name' key must be mapped to an object
        of type `str`, which must be the name of the dataset file, with the
        extension included. The 'cols' key must be mapped to a list of `dict`s,
        where each dictionary must have the following keys:

            (a) 'name' -> str, which will be the column name to be included;
            (b) 'dtype' -> type, which will be the type of that column in the
                    DataFrame;
            (c) 'converters' -> function, which will be the function to be
                    applied in this column for converting values before adding
                    it to the DataFrame;
            (d) 'preprocessor' -> function, which will be the function to be
                    applied in this column for converting values after adding it
                    to the DataFrame;
            (e) 'rename' -> str, which will be the new name for that column
                    after adding it to the DataFrame.

        All keys are optional except the 'name' key.

    Returns
    -------
    pandas.DataFrame
        A DataFrame containing the columns and settings defined by
        `file_configs`.
    """
    read_file = functools.partial(pd.read_csv, header=0, sep=';')

    df = None
    for file_config in file_configs:
        file_name = file_config['name']
        file_cols = file_config['cols']

        df_file = read_file(f"{path}/{file_name}",
            usecols=[col['name'] for col in file_cols],
            dtype={col['name']: col['dtype'] for col in file_cols if 'dtype' in col},
            converters={col['name']: col['converters'] for col in file_cols if 'converters' in col},
        )

        for col in file_cols:
            col_name = col['name']

            try:
                col_preproc = col['preprocessor']

            except KeyError:
                pass

            else:
                df_file[col_name] = col_preproc(df_file[col_name])

            try:
                col_new_name = col['rename']

            except KeyError:
                pass

            else:
                df_file.rename(columns={col_name: col_new_name}, inplace=True)

        df = df_file if df is None else pd.merge(df, df_file)

    return df

def extract_curves(df):
    def get_time_variation(timestamps: np.ndarray, 
            convert_to_seconds: bool = True) -> np.ndarray:
        """
        Normalizes a datetime array by the first element.
        
        Considering a array with ordered dates, each element is subtracted from
        the first element and added to a new array.
        
        Parameters
        ----------
        timestamps : numpy.ndarray
            Datetime array, ascending ordered.
            
        convert_to_seconds : bool, optional
            Defines whether to convert values to seconds after subtraction.
            Useful when the values in 'timestamps' are of the type
            `np.timedelta64`, since their subtractions result in unit values 'ns'.
            
        Returns
        -------
        numpy.ndarray
            Array of `int`, where each element is the difference between its
            value and the value of the first element, in seconds if
            `convert_to_seconds` else in nanoseconds.
        """
        c = np.timedelta64(1, 's') if convert_to_seconds else 1
        variations = [(timestamp - timestamps[0])/c for timestamp in timestamps]
        return np.array(variations)

    def is_minimum_set_length(values: list, min_length: int) -> bool:
        """
        Checks whether a list has a minimum value of distinct items.
        
        Examples
        --------
        >>> is_minimum_set_length([1, 2, 3], 2)
        True
        
        >>> is_minimum_set_length([1, 2, 2], 2)
        True
        
        >>> is_minimum_set_length([1, 1, 1, 2], 3)
        False
        
        Parâmetros
        ----------
        values : iterável
            Iterável a ser verificado.
            
        min_length : int
            Valor mínimo de itens distintos.
            
        Retorno
        ----------
        bool
            Se o iterável possui `min_length` itens distintos.
        """
        temp_set = set()
        for value in values:
            temp_set.add(value)
            
            if len(temp_set) > min_length:
                return True
            
        return False

    def get_device_curves(df=None, device_id=None, data=None, 
            timestamp_filter=None, curve_filter=None, state=None):
        if data is not None:
            timestamps, levels, states = data
            
        else:
            if df is None:
                raise ValueError("caso 'data' seja None, 'df'"
                            + " deverá ser passado como parâmetro")
                
            if device_id is None:
                raise ValueError("caso 'data' seja None, 'device_id'"
                            + " deverá ser passado como parâmetro")
                
            df_ = df[df['device_id'] == device_id]
            timestamps = df_['timestamp'].values
            levels = df_['battery_level'].values
            states = df_['battery_state'].values
        
        if state is not None:
            states, indexes = Array(states).filter_by(
                lambda x: x == state, return_indexes=True)
            levels = Array(levels).filter_by_indexes(indexes)
        
        curves, indexes = split_curves(levels, states,
                     charging_label=True, return_indexes=True)
        timestamps = Splitter(timestamps).by_tuple_indexes(indexes)
        timestamps = [get_time_variation(timestamp,
                 convert_to_seconds=data is None) for timestamp in timestamps]
            
        if timestamp_filter is not None:
            timestamps, indexes = Array(timestamps).filter_by(
                timestamp_filter, return_indexes=True)
            curves = Array(curves).filter_by_indexes(indexes)
        
        if curve_filter is not None:
            curves, indexes = Array(curves).filter_by(
                curve_filter, return_indexes=True)
            timestamps = Array(timestamps).filter_by_indexes(indexes)
                
        assert all(len(timestamp) == len(curve) for \
                timestamp, curve in zip(timestamps, curves))

        return timestamps, curves

    """
    Para simplificar os cálculos, será criado um novo DataFrame com modificações
    nas colunas 'device_id' (onde será composta de identificadores de
    dispositivos, porém sem repetições) e 'timestamp', 'battery_state' e
    'battery_level' (onde serão compostas de vetores, cujos tipos dos seus
    elementos serão 'np.timedelta64', 'bool' e 'int', respectivamente.
    As demais colunas continuarão representando seus respectivos dispositivos.
    """
    groups = df.groupby('device_id')
    df_ = pd.DataFrame()
    for column in df.columns:
        if column in ('timestamp', 'battery_state', 'battery_level'):
            df_[column] = groups[column].apply(np.array)
        else:
            df_[column] = groups[column].apply(lambda x: x.unique()[0])

    """
    Será ordenado cada vetor da coluna 'timestamp' de forma crescente. Como as
    colunas 'battery_state' e 'battery_level' possuem vetores paralelos aos
    vetores da coluna 'timestamp', então eles serão ordenados da mesma forma.
    """
    for i, row in df_.iterrows():
        indices = np.argsort(row['timestamp'])
        df_.at[i, 'timestamp'] = row['timestamp'][indices]
        df_.at[i, 'battery_state'] = row['battery_state'][indices]
        df_.at[i, 'battery_level'] = row['battery_level'][indices]

    """
    Para cada vetor da coluna 'timestamp', será subtraído o primeiro elemento
    do vetor de cada valor do vetor (isto é, 'x - x[0]' na notação NumPy) e,
    como o resultado da subtração gerará um resultado em nanossegundos, a
    unidade dos valores será alterada para segundos.
    """
    df_['timestamp'] = df_['timestamp'] \
                            .apply(lambda x: x - x[0]) \
                            .apply(lambda x: x / np.timedelta64(1, 's'))

    """
    A partir dos dados de 'timestamp', 'battery_state' e 'battery_level', serão
    extraídas as curvas do dispositivo, que serão geradas por certas condições
    e armazenadas em um novo DataFrame:

        - cada curva será ou de carregamento ou de descarregamento, então os
            dados 

                timestamp={0,1,2,3} ,
                battery_state={0,0,1,1}  e 
                battery_level={38,37,40,42}

            gerarão as curvas {({0,1},{38,37}),({2,3},{40,42})}. Percebe-se que
            os dados de 'timestamp' e 'battery_level' foram divididos quando
            'battery_state' era sequencialmente 0 ou 1.

        - cada curva será estritamente crescente ou estritamente decrescente,
            então os dados 

                timestamp={4,5,6,7} ,
                battery_state={0,0,0,0}  e 
                battery_level={37,36,41,40}

            gerarão as curvas {({4,5},{37,36}),({6,7},{41,40})}. Percebe-se que,
            mesmo com os dados de 'battery_state' sendo idênticos, os dados de
            'timestamp' e 'battery_level' foram divididos quando 'battery_level'
            era apenas decrescente ─ caso fossem considerado todos os valores,
            os valores 36 e 41 ainda seriam crescentes.

    Durante o processo de obtenção das curvas, também é possível aplicar certos
    filtros. Os filtros utilizados são que cada curva deve ter um tamanho mínimo
    de 5 pontos (esse valor foi escolhido pelo fato de o ajuste de curvas
    utilizar um polinômio de grau 4, portanto deverá ter pelo menos 5 pontos
    para que não ocorra ruído na análise). Então as curvas ({0,1,2},{0,2,4}) 
    não entrariam na análise, mas as curvas ({3,4,5,6,7},{9,16,25,36,49}) 
    entrariam, visto que possuem 5 pontos cada.
    """
    TIMESTAMP_INDEX, BATTERY_STATE_INDEX, BATTERY_LEVEL_INDEX = \
            df_.columns.get_loc('timestamp'), \
            df_.columns.get_loc('battery_state'), \
            df_.columns.get_loc('battery_level')

    df_curves = pd.DataFrame()
    df_curves['device_id'] = df_.index
    df_curves.set_index('device_id', inplace=True)
    df_curves['x'], df_curves['y'] = df_.apply(
        lambda cols: get_device_curves(
            data=(cols[TIMESTAMP_INDEX], cols[BATTERY_LEVEL_INDEX], cols[BATTERY_STATE_INDEX]),
            timestamp_filter=lambda timestamp: is_minimum_set_length(timestamp, 5),
            curve_filter=lambda curve: is_minimum_set_length(curve, 5)
        ), axis=1, raw=True, result_type='expand'
    ).T.values
    
    remaining_cols = np.delete(df_.columns, \
        np.where(np.isin(df_.columns, \
            ('device_id', 'timestamp', 'battery_state', 'battery_level'))
        )
    )

    for col in remaining_cols:
        df_curves[col] = df_[col]

    return df_curves



def load_dataset(path, first_k=None, verbose=False):
    def get_time_variation(timestamps, convert_to_seconds=True):
        c = np.timedelta64(1, 's') if convert_to_seconds else 1
        variations = [(timestamp - timestamps[0])/c for timestamp in timestamps]
        return np.array(variations)

    def is_minimum_set_length(values, min_length):
        temp_set = set()
        for value in values:
            temp_set.add(value)
            
            if len(temp_set) > min_length:
                return True
            
        return False

    def get_device_curves(df=None, device_id=None, data=None, 
            timestamp_filter=None, curve_filter=None, state=None):
        if data is not None:
            timestamps, levels, states = data
            
        else:
            if df is None:
                raise ValueError("caso 'data' seja None, 'df'"
                            + " deverá ser passado como parâmetro")
                
            if device_id is None:
                raise ValueError("caso 'data' seja None, 'device_id'"
                            + " deverá ser passado como parâmetro")
                
            df_ = df[df['device_id'] == device_id]
            timestamps = df_['timestamp'].values
            levels = df_['battery_level'].values
            states = df_['battery_state'].values
        
        if state is not None:
            states, indexes = Array(states).filter_by(
                lambda x: x == state, return_indexes=True)
            levels = Array(levels).filter_by_indexes(indexes)
        
        curves, indexes = split_curves(levels, states,
                     charging_label=True, return_indexes=True)
        timestamps = Splitter(timestamps).by_tuple_indexes(indexes)
        timestamps = [get_time_variation(timestamp,
                 convert_to_seconds=data is None) for timestamp in timestamps]
            
        if timestamp_filter is not None:
            timestamps, indexes = Array(timestamps).filter_by(
                timestamp_filter, return_indexes=True)
            curves = Array(curves).filter_by_indexes(indexes)
        
        if curve_filter is not None:
            curves, indexes = Array(curves).filter_by(
                curve_filter, return_indexes=True)
            timestamps = Array(timestamps).filter_by_indexes(indexes)
                
        assert all(len(timestamp) == len(curve) for \
                timestamp, curve in zip(timestamps, curves))

        return timestamps, curves

    read_file = functools.partial(pd.read_csv, header=0, sep=';')
    log = lambda msg: print(f'[{datetime.datetime.now():%H:%M:%S}] {msg}') if verbose else None

    dfs = []
    for file_config in file_configs:
        file_name = file_config['name']
        file_cols = file_config['cols']
        df_file = read_file(f"{path}/{file_name}",
            usecols=[col['name'] for col in file_cols],
            dtype={col['name']: col['dtype'] for col in file_cols if 'dtype' in col},
            converters={col['name']: col['converters'] for col in file_cols if 'converters' in col},
        )

        for col in file_cols:
            col_name = col['name']

            try:
                col_preproc = col['preprocessor']

            except KeyError:
                pass

            else:
                df_file[col_name] = col_preproc(df_file[col_name])

            try:
                col_new_name = col['rename']

            except KeyError:
                pass

            else:
                df_file.rename(columns={col_name: col_new_name}, inplace=True)

        dfs.append(df_file)

    df = dfs[0]
    for df_ in dfs[1:]:
        df = pd.merge(df, df_)




    log("Loading 'samples.csv'")
    df_samples = read_file(f'{path}/samples.csv',
        usecols=('id', 'device_id', 'timestamp', 'battery_state', 'battery_level'),
        dtype={'id': np.uint32, 'device_id': np.uint32},
        converters={
            'battery_state': lambda x: x in ('Charging', 'Full'),
            'battery_level': lambda x: int(float(x) * 100),
        },
    )

    log("Loading 'devices.csv'")
    df_devices = read_file(
        f'{PATH}/devices.csv',
        usecols=('id', 'model', 'brand'),
        converters={'model': str.lower, 'brand': str.lower},
    )

    log("Merging 'samples.csv' with 'devices.csv'")
    df_devices.rename(columns={'id': 'device_id'}, inplace=True)
    df = pd.merge(df_samples, df_devices)
    df['timestamp'] = pd.to_datetime(df['timestamp'])

    log('Dividing by groups of devices')
    df = df[df['battery_state'] == False]
    groups = df.groupby('device_id')
    df = pd.DataFrame()
    df['brand'] = groups['brand'].apply(lambda x: x.unique()[0])
    df['timestamp'] = groups['timestamp'].apply(np.array)
    df['battery_state'] = groups['battery_state'].apply(np.array)
    df['battery_level'] = groups['battery_level'].apply(np.array)

    for i in df.index:
        row = df.loc[i]
        indices = np.argsort(row['timestamp'])
        row['timestamp'] = row['timestamp'][indices]
        row['battery_state'] = row['battery_state'][indices]
        row['battery_level'] = row['battery_level'][indices]

    df['timestamp'] = df['timestamp']  \
            .apply(lambda x: x - x[0]) \
            .apply(lambda x: x / np.timedelta64(1, 's'))

    log("Creating 'df_curves'")
    df_curves = pd.DataFrame()
    df_curves['device_id'] = df.index
    df_curves.set_index('device_id', inplace=True)
    df_curves['brand'] = df['brand']
    df_curves['x'], df_curves['y'] = zip(*df.apply(
        lambda cols: get_device_curves(
            data=(cols[1], cols[3], cols[2]),
            timestamp_filter=lambda t: is_minimum_set_length(t, 3),
            curve_filter=lambda c: is_minimum_set_length(c, 3)
        ), axis=1, raw=True, result_type='expand')
    )
    df_curves['size'] = df_curves['x'].apply(len)

    log("Creating 'df_brands'")
    groups = df_curves.groupby('brand')
    df_brands = pd.DataFrame()
    df_brands['num_devices'] = groups['size'].count()
    df_brands['num_curves'] = groups['size'].sum()
    df_brands['x'] = groups['x'].sum()
    df_brands['y'] = groups['y'].sum()
    df_brands.sort_values('num_curves', ascending=False, inplace=True)

    if first_k is None:
        return df_brands

    return df_brands.head(first_k)
"""
if __name__ == '__main__':
    df = load_dataset('/home/enzo/harddrive/GreenHub', first_k=4, verbose=True)
"""