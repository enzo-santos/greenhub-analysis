if __name__ == '__main__':
    from array import Array
else:
    from .array import Array

class Splitter:
    """
    Classe wrapper com métodos úteis para manipulação de divisões em listas.
    """
    def __init__(self, array):
        self.array = array
      
    def by_tuple_indexes(self, indexes):
        """
        Divide o vetor encapsulado baseado em um vetor de índices.

        Cada valor do vetor de entrada deve ser uma tupla cujo
        primeiro elemento corresponde ao primeiro índice da
        divisão e o segundo elemento ao segundo índice da divisão.
        
        Exemplos
        ----------
        >>> s = Splitter([5, 2, 4, 7, 4, 3, 1, 0, 3])
        >>> s.by_indexes([(0, 4), (3, 6), (4, 9)])
        [[5, 2, 4, 7], [7, 4, 3], [4, 3, 1, 0, 3]]
        
        Parâmetros
        ----------
        indexes : list
            Vetor de tuplas contendo os índices pelos quais o
            vetor encapsulado será dividido.
            
        Retorno
        ----------
        list
            Subvetores do vetor encapsulado.
        """
        return [self.array[i:j] for i, j in indexes]

    def by_indexes(self, indexes):
        """
        Divide o vetor encapsulado baseado em um vetor de índices.
        
        Exemplos
        ----------
        >>> s = Splitter([5, 2, 4, 7, 4, 3, 1, 0, 3])
        >>> s.by_indexes([0, 4, 6, 9])
        [[5, 2, 4, 7], [4, 3], [1, 0, 3]]
        
        Parâmetros
        ----------
        indexes : list
            Vetor de inteiros contendo os índices pelos quais o
            vetor encapsulado será dividido. Esse vetor deverá
            estar ordenado. Caso seu primeiro elemento seja
            diferente de 0 (índice correspondente ao primeiro
            elemento) ou seu último elemento seja diferente de
            `len(self.array)`, seus índices serão considerados
            implicitamente.
            
        Retorno
        ----------
        list
            Subvetores do vetor encapsulado.
        """
        if indexes[0] != 0:
            indexes.insert(0, 0)

        if indexes[-1] != len(self.array):
            indexes.append(len(self.array))

        return [self.array[i:j] for i, j in zip(indexes[:-1], indexes[1:])]

    def by_inflection_points(self, return_indexes=False):
        """
        Divide o vetor encapsulado baseado em seus pontos de inflexão.
        
        Um ponto de inflexão é o valor no qual os valores do vetor
        alteram de crescente para decrescente ou vice-versa.
        
        Exemplos
        ----------
        >>> s = Splitter([1, 3, 7, 9, 6, 2, 5, 8, 10])
        >>> s.by_inflection_points()
        [[1, 3, 7, 9], [9, 6, 2], [2, 5, 8, 10]]
        
        Parâmetros
        ----------
        return_indexes : bool = False
            Define se devem ser retornados os índices os quais 
            ocorreram a divisão. 
            
        Retorno
        ----------
        list
            Subvetores do vetor encapsulado. Caso `return_indexes`
            seja `True`, será retornado uma tupla onde o segundo
            elemento são os índices da divisão.
        """
        sign = lambda x: -1 if x < 0 else 1 if x > 0 else 0

        splits = []
        indexes = []

        start = 0
        for i in range(1, len(self.array)-1):
            left_slope = sign(self.array[i] - self.array[i-1])
            right_slope = sign(self.array[i+1] - self.array[i])

            if left_slope != right_slope:
                splits.append(self.array[start:i+1])
                indexes.append((start, i+1))

                start = i

        if start < len(self.array):
            splits.append(self.array[start:])
            indexes.append((start, len(self.array)))

        if not return_indexes:
            return splits

        return splits, indexes

if __name__ == '__main__':
    import unittest

    class SplitterTest(unittest.TestCase):
        def test_by_inflection_points(self):
            # caso normal
            s = Splitter([1, 2, 3, 4, 3, 2, 1, 2, 3, 4])

            result = s.by_inflection_points()
            expected = [[1, 2, 3, 4], [4, 3, 2, 1], [1, 2, 3, 4]]
            self.assertEqual(result, expected)

            result = s.by_inflection_points(return_indexes=True)
            expected = ([[1, 2, 3, 4], [4, 3, 2, 1], [1, 2, 3, 4]], [(0, 4), (3, 7), (6, 10)])
            self.assertEqual(result, expected)

            # vetor crescente, sem pontos de inflexão,
            # nesse caso, retorna apenas o vetor encapsulado
            s = Splitter([1, 2, 3, 6, 11, 23, 47, 106, 235])

            result = s.by_inflection_points()
            expected = [[1, 2, 3, 6, 11, 23, 47, 106, 235]]
            self.assertEqual(result, expected)

            result = s.by_inflection_points(return_indexes=True)
            expected = ([[1, 2, 3, 6, 11, 23, 47, 106, 235]], [(0, 9)])
            self.assertEqual(result, expected)

            # vetor decrescente, sem pontos de inflexão,
            # nesse caso, retorna apenas o vetor encapsulado
            s = Splitter([20, 18, 16, 14, 12, 10, 9, 8])

            result = s.by_inflection_points()
            expected = [[20, 18, 16, 14, 12, 10, 9, 8]]
            self.assertEqual(result, expected)

            result = s.by_inflection_points(return_indexes=True)
            expected = ([[20, 18, 16, 14, 12, 10, 9, 8]], [(0, 8)])
            self.assertEqual(result, expected)

            # vetor misturado
            s = Splitter([1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 2, 2, 1, 1, 0, 0, 0, 0])
            result = s.by_inflection_points()
            expected = [[1, 0], [0, 1], [1, 0], [0, 0], [0, 1], [1, 1], [1, 0], [0, 0, 0], [0, 2], [2, 2], [2, 1], [1, 1], [1, 0], [0, 0, 0, 0]]
            self.assertEqual(result, expected)

    unittest.main()