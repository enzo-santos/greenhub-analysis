import multiprocessing as mp

def apply_async(function, inputs):
    with mp.Pool(processes=2) as pool:
        results = [pool.apply_async(function, args=(input_,)) for input_ in inputs]
    return [result.get() for result in results]