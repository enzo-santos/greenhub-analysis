class Array:
    """
    Classe wrapper com métodos úteis para manipulação de listas.
    """
    def __init__(self, array):
        """
        Inicializa o vetor encapsulado.
        """
        self.array = array
            
    def map_by(self, predicate):
        """
        Transforma os elementos do vetor encapsulado por predicado.

        Exemplos
        ----------
        >>> a = Array([1, 2, 3, 4, 5])
        >>> a.map_by(lambda x: x * x)
        [1, 4, 9, 16, 25]

        Parâmetros
        ----------
        predicate : function
            Função que irá mapear cada elemento do vetor encapsulado
            em outro valor. A função deve receber como entrada apenas
            um argumento, que será utilizado para passar como entrada
            os elementos do vetor, e deve retornar o novo valor que
            será usado para substituir o valor original.

        Retorno
        ----------
        list
            Lista contendo os valores mapeados.
        """
        return [predicate(value) for value in self.array]

    def filter_by(self, predicate, return_indexes=False):
        """
        Filtra os elementos do vetor encapsulado por predicado.

        A função `filter_by` possui como predicado uma função que recebe
        como entrada apenas um argumento, que é o elemento do vetor encapsulado.
        Já a função `filter_by_with_index` possui como predicado uma função
        que recebe como entrada dois argumentos, que é o índice do elemento do 
        vetor encapsulado e o próprio elemento, respectivamente.

        Exemplos
        ----------
        >>> a = Array([1, 7, 3, 2, 6, 4])
        >>> a.filter_by(lambda x: x < 5)
        [1, 3, 2, 4]
        >>> a.filter_by(lambda x: x < 5, return_indexes=True)
        ([1, 3, 2, 4], [0, 2, 3, 5])

        Parâmetros
        ----------
        predicate : function
            Função que irá filtrar cada elemento do vetor encapsulado.
            A função deve receber como entrada apenas um argumento, que
            será utilizado para passar como entrada os elementos do vetor,
            e deve retornar `bool` que indicará se o elemento permanecerá
            no vetor de retorno após a filtragem (`True`).

        return_indexes : bool = False
            Define se deverá ser retornado os índices, em relação ao
            vetor encapsulado, dos elementos que permaneceram após a
            filtragem. 

        Retorno
        ----------
        list / tuple
            Lista contendo os elementos filtrados. Caso `return_indexes`
            seja `True`, será retornado uma tupla em que o segundo elemento
            corresponde aos índices dos valores que permaneceram após o filtro.
        """
        if not return_indexes:
            return [x for x in self.array if predicate(x)]

        filtered_values = [(i, x) for i, x in enumerate(self.array) if predicate(x)]
        return [v[1] for v in filtered_values], [v[0] for v in filtered_values]

    def filter_by_with_index(self, predicate, return_indexes=False):
        """
        Filtra os elementos do vetor encapsulado por predicado.

        A função `filter_by` possui como predicado uma função que recebe
        como entrada apenas um argumento, que é o elemento do vetor encapsulado.
        Já a função `filter_by_with_index` possui como predicado uma função
        que recebe como entrada dois argumentos, que é o índice do elemento do 
        vetor encapsulado e o próprio elemento, respectivamente.

        Exemplos
        ----------
        >>> a = Array([1, 5, 3, 2, 4])
        >>> a.filter_by_with_index(lambda i, x: i % 2 == 0 and x < 4)
        [1, 3]

        Parâmetros
        ----------
        predicate : function
            Função que irá filtrar cada elemento do vetor encapsulado.
            A função deve receber como entrada dois argumentos, que
            será utilizado para passar como entrada os índices de cada
            elemento do vetor e o próprio elemento, respectivamente, e
            deverá retornar `bool` que indicará se o elemento permanecerá
            no vetor de retorno após a filtragem (`True`).

        return_indexes : bool = False
            Define se deverá ser retornado os índices, em relação ao
            vetor encapsulado, dos elementos que permaneceram após a
            filtragem. 

        Retorno
        ----------
        list / tuple
            Lista contendo os elementos filtrados. Caso `return_indexes`
            seja `True`, será retornado uma tupla em que o segundo elemento
            corresponde aos índices dos valores que permaneceram após o filtro.
        """
        if not return_indexes:
            return [x for i, x in enumerate(self.array) if predicate(i, x)]

        filtered_values = [(i, x) for i, x in enumerate(self.array) if predicate(i, x)]
        return [v[1] for v in filtered_values], [v[0] for v in filtered_values]
    
    def filter_by_indexes(self, indexes):
        """
        Filtra os elementos do vetor encapsulado por índices.

        Exemplos
        ----------
        >>> a = ArrayUtil([1, 2, 3, 4, 5])
        >>> a.filter_by_indexes([0, 1, 3])
        [1, 2, 4]

        Parâmetros
        ----------
        indexes : list
            Índices dos elementos que devem permanecer no vetor após o filtro.

        Retorno
        ----------
        list
            Lista contendo os elementos filtrados.
        """
        return [x for i, x in enumerate(self.array) if i in indexes]

    def is_increasing(self):
        """
        Verifica se todos os elementos do vetor estão crescendo ou decrescendo.
        
        Exemplos
        ----------
        >>> Array([1, 2, 3]).is_increasing()
        True
        
        >>> Array([7, 2, 1]).is_increasing()
        False
        
        >>> Array([4, 3, 2, 3]).is_increasing()
        ValueError('input is not a monotonic sequence')
        
        Retorno
        ----------
        bool
            Se os elementos estão crescendo.
        """
        if len(self.array):
            if all(e1 <= e2 for e1, e2 in zip(self.array[:-1], self.array[1:])):
                return True

            if all(e1 >= e2 for e1, e2 in zip(self.array[:-1], self.array[1:])):
                return False

        raise ValueError('input is not a monotonic sequence')

    def is_constant(self):
        """
        Verifica se os elementos do vetor possuem o mesmo valor.

        Exemplos
        ----------
        >>> Array([1, 1, 1, 1, 1]).is_constant()
        True
        >>> Array([2, 3, 4, 5, 6]).is_constant()
        False

        Retorno
        ----------
        bool
            Se os elementos possuem o mesmo valor.
        """
        return all(value == self.array[0] for value in self.array)

    def is_monotonic(self):
        """
        Verifica se os elementos de um vetor estão crescendo xou decrescendo.

        Exemplos
        ----------
        >>> Array([1, 2, 3, 4, 5]).is_monotonic()
        True
        >>> Array([1, 2, 3, 2, 1]).is_monotonic()
        False

        Retorno
        ----------
        bool
            Se os elementos formam uma sequência monotônica.
        """
        try:
            self.is_increasing()
            return True

        except ValueError:
            return False

    def strip_duplicates(self):
        """
        Reduz elementos consecutivos que estão repetidos no vetor em apenas um.

        Exemplos
        ----------
        >>> Array([1, 1, 1, 2, 3, 3, 4]).strip_duplicates()
        [1, 2, 3, 4]
        >>> Array([1, 1, 1, 1, 2, 1, 1, 1]).strip_duplicates()
        [1, 2, 1]

        Retorno
        ----------
        list
            Lista contendo as duplicatas consecutivas únicas.
        """
        return [v for i, v in enumerate(self.array) if i == 0 or v != self.array[i-1]]

    def can_access(self, index):
        """
        Verifica se é possível o vetor acessar um índice com sucesso.

        Exemplos
        ----------
        >>> Array([7, 3, 5, 2]).can_access(1)
        True
        >>> Array([6, 5, 1, 3]).can_access(6)
        False

        Parâmetros
        ----------
        index : int
            O índice a ser verificado.

        Retorno
        ----------
        bool
            Se é possível o vetor acessar o índice especificado sem erros.
        """
        try:
            self.array[index]
            return True

        except IndexError:
            return False

if __name__ == '__main__':
    import unittest

    class ArrayTest(unittest.TestCase):
        def test_filter_by(self):
            a = Array([1, 7, 3, 2, 6, 4])

            # caso normal
            result = a.filter_by(lambda x: x < 5)
            expected = [1, 3, 2, 4]
            self.assertEqual(result, expected)

            result = a.filter_by(lambda x: x < 5, return_indexes=True)
            expected = ([1, 3, 2, 4], [0, 2, 3, 5])
            self.assertEqual(result, expected)
        
            # o filtro não se aplica a nenhum elemento
            # nesse caso, retorna um vetor vazio
            result = a.filter_by(lambda x: x < 0)
            expected = []
            self.assertEqual(result, expected)

            result = a.filter_by(lambda x: x < 0, return_indexes=True)
            expected = ([], [])
            self.assertEqual(result, expected)

            # o filtro se aplica a todos os elementos
            # nesse caso, retorna o vetor encapsulado
            result = a.filter_by(lambda x: x < 10)
            expected = [1, 7, 3, 2, 6, 4]
            self.assertEqual(result, expected)

            result = a.filter_by(lambda x: x < 10, return_indexes=True)
            expected = ([1, 7, 3, 2, 6, 4], [0, 1, 2, 3, 4, 5])
            self.assertEqual(result, expected)

        def test_filter_by_indexes(self):
            a = Array([1, 7, 3, 2, 6, 4])

            # caso normal
            result = a.filter_by_indexes([0, 1, 4])
            expected = [1, 7, 6]
            self.assertEqual(result, expected)

            # alguns dos índices estão fora do limite
            # nesse caso, ignorar
            result = a.filter_by_indexes([2, 3, 10])
            expected = [3, 2]
            self.assertEqual(result, expected)

            # todos os índices estão fora do limite
            # nesse caso, ignorar todos, retorna um vetor vazio
            result = a.filter_by_indexes([6, 8, 11])
            expected = []
            self.assertEqual(result, expected)

        def test_is_increasing(self):
            # vetor crescendo constantemente (progressão aritmética)
            a = Array([1, 2, 3, 4, 5, 6])
            self.assertTrue(a.is_increasing())

            # vetor crescendo não constantemente
            a = Array([2, 3, 5, 7, 9, 11, 13])
            self.assertTrue(a.is_increasing())

            # vetor decrescendo constantemente (progressão aritmética)
            a = Array([6, 5, 4, 3, 2, 1])
            self.assertFalse(a.is_increasing())

            # vetor decrescendo não constantemente
            a = Array([64, 32, 16, 8, 4, 2, 1])
            self.assertFalse(a.is_increasing())

            # vetor é constante
            # nesse caso, a função retornará True
            a = Array([1, 1, 1, 1, 1, 1, 1, 1])
            self.assertTrue(a.is_increasing())

            # vetor não é monotônico (está crescendo e decrescendo)
            a = Array([5, 3, 1, 5, 3, 2, 4, 5])
            self.assertRaises(ValueError, a.is_increasing)

            # vetor não é monotônico no último elemento
            a = Array([1, 2, 3, 4, 5, 6, 7, 6])
            self.assertRaises(ValueError, a.is_increasing)

            # vetor não é monotônico no primeiro elemento
            a = Array([10, 1, 2, 3, 4, 5, 6, 7])
            self.assertRaises(ValueError, a.is_increasing)

            # todos os elementos são constantes exceto o último
            a = Array([0, 0, 0, 0, 0, 0, 1])
            self.assertTrue(a.is_increasing())

            a = Array([0, 0, 0, 0, 0, 0, -1])
            self.assertFalse(a.is_increasing())

            # todos os elementos são constantes exceto o primeiro
            a = Array([0, 1, 1, 1, 1, 1, 1])
            self.assertTrue(a.is_increasing())

            a = Array([2, 1, 1, 1, 1, 1, 1])
            self.assertFalse(a.is_increasing())  

            # vetor vazio
            a = Array([])
            self.assertRaises(ValueError, a.is_increasing)

    unittest.main()