from .util.array import Array
from .util.splitter import Splitter

def split_curves(levels, states, charging_label=1, return_indexes=False):
    curves = []
    curves_indexes = []
    levels_indexes = [i for i in range(len(levels))]

    # Retorna os índices nos quais elementos consecutivos
    # trocam se alternam de valor dentro do vetor 'states'
    indexes = [0] + [next_index for actual_value, (next_index, next_value)
                    in zip(states[:-1], enumerate(states[1:], start=1))
                    if actual_value != next_value] + [len(states)]

    splits_by_state = Splitter(levels).by_indexes(indexes)
    index_splits_by_state = Splitter(levels_indexes).by_indexes(indexes)

    unique_states = Array(states).strip_duplicates()
    for split, state, indexes in zip(splits_by_state, unique_states, index_splits_by_state):
        def _filter(x):
            # Mantém apenas os valores que estão crescendo e seu
            # estado correspondente é carregando xou os valores que
            # estão decrescendo e seu estado correspondente é descarregando
            if Array(x).is_constant():
                return True

            return not (Array(x).is_increasing() ^ (state == charging_label))

        splits, indexes_splits = Splitter(split).by_inflection_points(return_indexes=True)
        splits, indexes_filter = Array(splits).filter_by(_filter, return_indexes=True)

        filtered_indexes = Array(indexes_splits).filter_by_indexes(indexes_filter)

        indexes = Splitter(indexes).by_tuple_indexes(filtered_indexes)
        indexes = Array(indexes).map_by(lambda x: (x[0], x[-1]+1))

        curves.extend(splits)
        curves_indexes.extend(indexes)

    if not return_indexes:
        return curves

    return curves, curves_indexes
